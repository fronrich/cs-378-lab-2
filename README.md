# CS 378 Lab 2

# Team Members:

### - Henry Feng

### - Carlos Ovalle

### - Grayson Pike

### - Fronrich Puno

## Branching/Source control

###### There will be two main branches called production and develop. When testing, modifying, etc code will be pushed to the develop branch. Upon reaching a checkpoint and making sure develop is stable, the develop branch will be merged into production. When testing or implementing new features, branching will be done off of develop. This system will help ensure that we have a consistent and stable build of our project.
